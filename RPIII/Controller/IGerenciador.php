<?php
interface IGerenciador {
	public function criar();
	public function recuperar();
	public function deletar();
	public function  editar();
	
}